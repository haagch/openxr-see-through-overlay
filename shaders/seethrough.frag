/*
 * OpenXR Video Seethrough Overlay
 *
 * Copyright 2019 Collabora Ltd.
 *
 * Authors: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#version 460 core

layout(set = 0, binding = 0) uniform UBO
{
  int view_index;
} ubo;

layout(set = 0, binding = 1) uniform sampler2D map;

layout(location = 0) in vec2 in_uv;

layout(location = 0) out vec4 out_color;


const vec2 center[2] = { vec2(476.2185398810301, 483.76209342457366),
                         vec2(498.00415451344821, 482.98584986781174) };
const vec2 focal[2] = { vec2(416.35145946203608, 416.51910754889661),
                        vec2(415.55983009619729, 415.74783164673471) };

const vec4 coeffs[2] = { vec4(0.18768459999438458, 0.045326083206918687,
                             -0.22674478566835726, 0.091481914849107199),
                         vec4(0.19514125443998429, 0.020182932770895591,
                             -0.19687865856943762, 0.079536991897326118) };

const ivec2 resolution = ivec2(960, 960);

void
main()
{
  const vec2 f = focal[ubo.view_index];
  const vec2 c = center[ubo.view_index];
  const vec4 d = coeffs[ubo.view_index];

  const mat3 K = mat3(f[0], 0,    c[0],
                      0,    f[1], c[1],
                      0,    0,    1);

  const mat3 iK = inverse(K);

  vec2 pixel_coord = resolution * in_uv;

  vec2 xy = vec2(
    pixel_coord.x * iK[0][0] + pixel_coord.y * iK[0][1] + iK[0][2],
    pixel_coord.x * iK[1][0] + pixel_coord.y * iK[1][1] + iK[1][2]);

  float r = length(xy);

  float theta = atan(r);

  float theta2 = theta * theta;
  float theta4 = theta2 * theta2;
  float theta6 = theta4 * theta2;
  float theta8 = theta4 * theta4;
  float theta_d = theta * (1 + d[0] * theta2 + d[1] * theta4
                             + d[2] * theta6 + d[3] * theta8);

  float scale = (r == 0.0) ? 1.0 : theta_d / r;

  vec2 uv_pixel = f * xy * scale + c;
  vec2 uv_undistorted = uv_pixel / resolution;


  vec2 uv = vec2(uv_undistorted.x / 2.0f, uv_undistorted.y);
  if (ubo.view_index == 1) {
    uv.x += 0.5f;
  }

  out_color = texture(map, uv);
}
