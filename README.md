![Screenshot](doc/screenshot.jpg "Screenshot")

# OpenXR Seethrough Overlay

Video seethrough overlay for OpenXR runtimes.

# Dependencies

* OpenXR headers and loader
* Vulkan headers and loader
* GStreamer
* shaderc
* gio

# License

OpenXR Seethrough Overlay is released under the MIT License.

# Build
```
$ meson build
```

```
$ ninja -C build
```

# Run

```
$ ./build/src/xr-seethrough-overlay
```

Run in Monado's XCB windowed mode, with Vulkan validation.

```
$ XR_RUNTIME_JSON=/path/to/openxr_monado-dev.json XRT_COMPOSITOR_FORCE_XCB=TRUE VK_INSTANCE_LAYERS=VK_LAYER_KHRONOS_validation ./build/src/xr-seethrough-overlay
```

# Commands

```
Options:
  -d, --device VIDEO_DEVICE    Video device path (default: /dev/video0)
  -h, --help                   Show this help
```
