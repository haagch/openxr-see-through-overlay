/*
 * OpenXR Video Seethrough Overlay
 *
 * Copyright 2017-2021 Collabora Ltd.
 *
 * Authors: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include <gst/gst.h>

#include "vulkan_framebuffer.h"
#include "log.h"
#include "xr.h"
#include "pipeline.h"
#include "settings.h"

#include "camera_gst.h"

struct _application
{
  settings settings;

  xr_example xr;

  VkInstance instance;
  vulkan_device *vk_device;

  pipeline pipe;
  vulkan_framebuffer **framebuffers[2];
  VkCommandBuffer *draw_cmd;

  VkCommandPool cmd_pool;
  VkQueue queue;
  VkPipelineCache pipeline_cache;

  camera_gst *cam;

  GMainLoop *loop;
};

static void
app_destroy(application *self)
{
  vkDeviceWaitIdle(self->vk_device->device);

  for (uint32_t i = 0; i < 2; i++) {
    for (uint32_t j = 0; j < self->xr.sky.swapchain_length[i]; j++)
      if (self->framebuffers[i][j]) {
        vulkan_framebuffer_destroy(self->framebuffers[i][j]);
        free(self->framebuffers[i][j]);
      }
    free(self->framebuffers[i]);
  }
  free(self->draw_cmd);
  pipeline_destroy(&self->pipe);

  xr_cleanup(&self->xr);

  vkDestroyPipelineCache(self->vk_device->device, self->pipeline_cache, NULL);

  vkDestroyCommandPool(self->vk_device->device, self->cmd_pool, NULL);

  vulkan_device_destroy(self->vk_device);
  vkDestroyInstance(self->instance, NULL);

  g_main_loop_unref(self->loop);

  log_d("Shut down");
}

static gboolean
_draw_cb(gpointer data)
{
  application *self = (application *)data;
  xr_begin_frame(&self->xr);

  uint32_t buffer_index[2] = { 0, 0 };
  for (uint32_t i = 0; i < 2; i++) {
    if (!xr_aquire_swapchain(&self->xr, &self->xr.sky, i, &buffer_index[i])) {
      log_e("Could not aquire xr swapchain");
      return FALSE;
    }
  }

  VkPipelineStageFlags stage_flags[1] = {
    VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT
  };

  VkSubmitInfo submit_info = {
    .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
    .pWaitDstStageMask = stage_flags,
    .commandBufferCount = 1,
  };

  submit_info.pCommandBuffers = &self->draw_cmd[buffer_index[0]];
  vk_check(vkQueueSubmit(self->queue, 1, &submit_info, VK_NULL_HANDLE));

  for (uint32_t i = 0; i < 2; i++) {
    if (!xr_release_swapchain(self->xr.sky.swapchains[i])) {
      log_e("Could not release xr swapchain");
      return FALSE;
    }
  }

  if (!xr_end_frame(&self->xr)) {
    log_e("Could not end xr frame");
  }
  return TRUE;
}

static VkCommandBuffer
_create_command_buffer(application *self)
{
  VkCommandBufferAllocateInfo info = {
    .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
    .commandPool = self->cmd_pool,
    .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
    .commandBufferCount = 1
  };

  VkCommandBuffer cmd_buffer;
  vk_check(
    vkAllocateCommandBuffers(self->vk_device->device, &info, &cmd_buffer));

  return cmd_buffer;
}

static void
app_build_command_buffer(application *self,
                         VkCommandBuffer *cb,
                         vulkan_framebuffer ***fb,
                         uint32_t view_count,
                         uint32_t swapchain_index,
                         pipeline *pipe)
{
  *cb = _create_command_buffer(self);

  VkCommandBufferBeginInfo command_buffer_info = {
    .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO
  };
  vk_check(vkBeginCommandBuffer(*cb, &command_buffer_info));

  for (uint32_t view_index = 0; view_index < view_count; view_index++) {
    vulkan_framebuffer_begin_render_pass(fb[view_index][swapchain_index], *cb);
    vulkan_framebuffer_set_viewport_and_scissor(fb[view_index][swapchain_index],
                                                *cb);

    pipeline_draw(pipe, *cb, view_index);

    vkCmdEndRenderPass(*cb);
  }

  vk_check(vkEndCommandBuffer(*cb));
}

static void
_create_pipeline_cache(application *self)
{
  VkPipelineCacheCreateInfo info = {
    .sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO
  };
  vk_check(vkCreatePipelineCache(self->vk_device->device, &info, NULL,
                                 &self->pipeline_cache));
}

static void
_create_command_pool(application *self, uint32_t index)
{
  VkCommandPoolCreateInfo cmd_pool_info = {
    .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
    .flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
    .queueFamilyIndex = index
  };

  vk_check(vkCreateCommandPool(self->vk_device->device, &cmd_pool_info, NULL,
                               &self->cmd_pool));
}

static void
_first_frame_cb(application *self)
{
  for (uint32_t i = 0; i < self->xr.sky.swapchain_length[0]; i++)
    app_build_command_buffer(self, &self->draw_cmd[i], self->framebuffers,
                             self->xr.view_count, i, &self->pipe);

  g_timeout_add(1, _draw_cb, self);
}

static bool
app_init(application *self, int argc, char *argv[])
{
  self->cam = camera_gst_new();
  camera_gst_set_first_frame_cb(self->cam, self, _first_frame_cb);

  self->loop = g_main_loop_new(NULL, FALSE);

  if (!settings_parse_args(&self->settings, argc, argv))
    log_f("Invalid arguments.");

  /*
   * vulkan_enable2 lets the runtime create a VkInstance and VkDevice, so we
   * let xr_init2 create context.instance and this->vk_device.
   */
  if (!xr_init2(&self->xr, &self->instance, &self->vk_device)) {
    log_e("OpenXR graphics initialization failed.");
    return false;
  }

  vkGetDeviceQueue(self->vk_device->device,
                   self->vk_device->graphics_family_index, 0, &self->queue);

  _create_pipeline_cache(self);
  _create_command_pool(self, 0);

  if (!xr_init_post_vk(
        &self->xr, self->instance, self->vk_device->physical_device,
        self->vk_device->device, self->vk_device->graphics_family_index, 0)) {
    log_e("OpenXR initialization failed.");
    return false;
  }
  log_i("Initialized OpenXR with %d views.", self->xr.view_count);

  for (uint32_t i = 0; i < self->xr.view_count; i++) {
    self->draw_cmd = (VkCommandBuffer *)malloc(
      sizeof(VkCommandBuffer) * self->xr.sky.swapchain_length[i]);

    self->framebuffers[i] = (vulkan_framebuffer **)malloc(
      sizeof(vulkan_framebuffer *) * self->xr.sky.swapchain_length[i]);
    for (uint32_t j = 0; j < self->xr.sky.swapchain_length[i]; j++) {
      self->framebuffers[i][j] =
        vulkan_framebuffer_create(self->vk_device->device);
      vulkan_framebuffer_init(
        self->framebuffers[i][j], self->vk_device,
        self->xr.sky.images[i][j].image, (VkFormat)self->xr.swapchain_format,
        self->xr.configuration_views[i].recommendedImageRectWidth,
        self->xr.configuration_views[i].recommendedImageRectHeight);
    }
  }

  pipeline_init(&self->pipe, self->vk_device,
                self->framebuffers[0][0]->render_pass, self->pipeline_cache);

  camera_gst_set_pipeline(self->cam, &self->pipe);

  camera_gst_init(self->cam, self->settings.device_path);

  return true;
}

static application app;
static void
sigint_cb(int signum)
{
  (void)signum;
  g_main_loop_quit(app.loop);
}

int
main(int argc, char *argv[])
{
  gst_init(&argc, &argv);

  if (!app_init(&app, argc, argv))
    return -1;

  signal(SIGINT, sigint_cb);

  g_main_loop_run(app.loop);

  app_destroy(&app);

  return 0;
}
